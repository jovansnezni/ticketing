package rs.ac.bg.etf.ticketing.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import rs.ac.bg.etf.ticketing.entity.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, Integer> {

    @Query("from Ticket t where t.soldAmount < t.amount")
    Iterable<Ticket> findAvailableTickets();
}
