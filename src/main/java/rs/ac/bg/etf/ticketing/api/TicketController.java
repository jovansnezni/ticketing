package rs.ac.bg.etf.ticketing.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.ac.bg.etf.ticketing.repository.TicketRepository;
import rs.ac.bg.etf.ticketing.entity.Ticket;

@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    TicketRepository ticketRepository;

    @GetMapping
    public Iterable<Ticket> getTickets() {
        return ticketRepository.findAll();
    }

    @GetMapping("/available")
    public Iterable<Ticket> getAvailableTickets() {
        return ticketRepository.findAvailableTickets();
    }

    @PostMapping("/buy/{id}")
    public void buyTicket(@PathVariable int id) {
        ticketRepository
                .findById(id)
                .filter(t -> t.getSoldAmount() < t.getAmount())
                .ifPresent(t -> {
                    t.setSoldAmount(t.getSoldAmount() + 1);
                    ticketRepository.save(t);
                });
    }
}
