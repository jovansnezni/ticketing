package rs.ac.bg.etf.ticketing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import rs.ac.bg.etf.ticketing.entity.Ticket;
import rs.ac.bg.etf.ticketing.repository.TicketRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class TicketingApplication {

    @Autowired
    private TicketRepository ticketRepository;

    public static void main(String[] args) {
        SpringApplication.run(TicketingApplication.class, args);

    }

    @PostConstruct
    private void initStartingData() {
        List<Ticket> tickets = new ArrayList<>();
        ticketRepository.findAll().forEach(tickets::add);

        if (tickets.isEmpty()) {
            ticketRepository.save(new Ticket("Belgrade", "Kragujevac", 40, 1));
            ticketRepository.save(new Ticket("Belgrade", "Obrenovac", 44, 2));
            ticketRepository.save(new Ticket("Belgrade", "Krusevac", 30, 3));
            ticketRepository.save(new Ticket("Belgrade", "Novi Sad", 22, 4));
            ticketRepository.save(new Ticket("Belgrade", "Lazarevac", 38, 5));
        }
    }
}
