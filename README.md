# Ticketing

# Payment

Sva potrebna konfiguracija aplikacije se nalazi unutar src/main/resources/applicayion.properties fajla.


    spring.jpa.hibernate.ddl-auto=update
	spring.datasource.url=jdbc:mysql://localhost:3306/ticketing?useSSL=false
	spring.datasource.username=root
	spring.datasource.password=root

	server.port=9000


Radi pokretanja aplikacije neophodno je postaviti parametre konekcije prema mysql bazi, username, root i url. 

U bazi mora da postoje shema **ticketing**, tabele ce biti samostalno kreirane nakon pokretanja.

Pri prvom pokretanju baza ce se napuniti test podacima, sto radi sledeca metoda:

	@PostConstruct
    private void initStartingData() {
        List<Ticket> tickets = new ArrayList<>();
        ticketRepository.findAll().forEach(tickets::add);

        if (tickets.isEmpty()) {
            ticketRepository.save(new Ticket("Belgrade", "Kragujevac", 40, 1));
            ticketRepository.save(new Ticket("Belgrade", "Obrenovac", 44, 2));
            ticketRepository.save(new Ticket("Belgrade", "Krusevac", 30, 3));
            ticketRepository.save(new Ticket("Belgrade", "Novi Sad", 22, 4));
            ticketRepository.save(new Ticket("Belgrade", "Lazarevac", 38, 5));
        }
    }


